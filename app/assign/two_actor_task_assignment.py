""" uses one-hot description for actions; state transitions are determined from logic-OR between the action and the
starting state

transition probabilities need to sum up to one, that is the joint probability of leaving state s_hat by taking action a_hat
so, you give me a number for success between 0,1 and i will calculate its complement (which means "failed to leave
this-state w/ this-action")
(ref. Sutton p.49 Ch.3 & Bertsekas exer. 1.3)

"""

from collections import defaultdict
from typing import Optional


class ProblemSetup:
    """
    builds the facilities used to generate the Value Function (i.e. BuildCostMatrix):

    - transition probability matrix: binds the two vectors in a dictionary labeled by the target state (we assume
    that the state transition probability depends only on the end-state (not on the arc that connects two states))
    looks like this         {'h': {'0b1': 0.9, '0b10': 0.7}, 'r': {'0b1': 0.7, '0b10': 0.7,..}

    - actions matrix: builds a list with all possible actions
    looks like this:         ['h0b1', 'h0b10', 'h0b100', 'h0b1000', ..., 'r0b1', 'r0b10', ..]
    """

    def __init__(self, human_success_prob_list: list[float] = None, robot_success_prob_list: list[float] = None,
                 start: str = None):

        """the following entities should be provided by the api and their dims are interdependent"""

        if human_success_prob_list is None:
            human_success_prob_list = [0.8, 0.0, 0.6]
        if robot_success_prob_list is None:
            robot_success_prob_list = [0.7, 0.7, 0.7]
        transition_prob = [human_success_prob_list, robot_success_prob_list]

        if len(transition_prob[0]) != len(transition_prob[1]):
            raise Exception("human and robot do not have the same number of transition probabilities "
                            "(do not depart from all states)")

        if start is None:
            start = '0b' + ''.join('0' for _ in range( len(transition_prob[0]) ))
        if len(start[2:]) != len(transition_prob[0]):
            raise Exception("start state description cannot accommodate all state transitions")

        # self.state_set = map(lambda x: bin(x), range(int(self.terminal_state, 2) + 1))
        self.state_start = start  # increase the size here, update the transition_probabilities manually and the time-horizon for the estimation
        self.time_horizon = len(transition_prob[0]) - 1  # N: solving for a new episode (clean piece of assembly)
        self.terminal_state = bin(2 ** (len(self.state_start) - 2) - 1)  # python does not have an easy bit-complement
        self.transition_probabilities = ProblemSetup.build_transition_prob_matrix(self.state_start, transition_prob)
        self.actions = ProblemSetup.get_all_actions(self.state_start)

    @staticmethod
    def build_transition_prob_matrix(state_start: str = '0b000', transition_prob: list[list[float], list[float]] = None
                                     ) -> dict[str: float]:
        human_success_prob_list, robot_success_prob_list = transition_prob
        actions = ProblemSetup.get_all_actions(state_start)
        transition_probabilities = {
            'h': dict(zip([action[1:] for action in actions], human_success_prob_list)),
            # probability of success starting from easy -> hard
            'r': dict(zip([action[1:] for action in actions], robot_success_prob_list))
        }
        return transition_probabilities

    @staticmethod
    def get_all_actions(start: str) -> list[str]:
        """
        start: '0b00'  (the start-state of the trellis graph)
        returns: ['h0b1', 'h0b10', 'r0b1', 'r0b10'] (human assigned to #1 (0b1), .., robot assigned to #2 (0b01))
        """
        sz = len(start) - 2
        first_task = int(start, 2) + 1
        return [i + j for i in ['h', 'r'] for j in
                map(lambda x: bin(x), [first_task << i for i in range(sz)])]  # 'i+j' means string append here


class BuildCostMatrix:
    def __init__(self, state_start, transition_prob, actions, horizon):
        self.rewards_table = defaultdict(dict)
        self.transition_probabilities = transition_prob
        self.actions = actions
        self.horizon = horizon

        self.build_cost_table(state_start)

    def build_cost_table(self, state_start):
        """
        'self.rewards_table' is populated from the recursive call to 'self.calc_cost_from_state()'
        """
        best_action, best_cost = self.calc_cost_from_state(state=state_start, cost_this_state={}, t=0)
        self.rewards_table = dict(self.rewards_table)

    def calc_cost_from_state(self, state: str, cost_this_state: dict, t) -> tuple:
        """
        calc the reward for *this* state by taking each possible action; since you don't know the reward of 
        future states, make a recursive call to calculate it on the fly    
        """

        if t > self.horizon:
            return 'no action', 0.

        for action in self.actions:
            if (int(state, 2) & int(action[1:], 2)) == int(action[1:], 2):
                # print(f'infeasible action: {state, action}')
                continue

            cost_this_state[action] = 0
            for succeed in (False, True):
                pij = self.get_pij(action, succeed)
                r = BuildCostMatrix.get_reward(succeed)
                next_state = BuildCostMatrix.get_next_state(state, action, succeed)

                future_cost = self.calc_cost_from_state(next_state, {}, t + 1)[1]
                cost_this_state[action] += pij * (r + future_cost)     # this is the Bellman equation
        # print(f'cost_this_state: {t, state, cost_this_state}')

        self.rewards_table[t][state] = cost_this_state
        best_action = max(cost_this_state, key=cost_this_state.get)
        best_cost = cost_this_state[best_action]
        return best_action, best_cost

    def get_pij(self, action: str, succeeded: bool) -> float:
        return self.transition_probabilities[action[0]][action[1:]] if succeeded \
            else 1. - self.transition_probabilities[action[0]][action[1:]]

    @staticmethod
    def get_next_state(start: str, action: str, succeeded: bool) -> str:
        """examples of input values
        this_state: '0b001'
        action: 'h0b001' (human + designated action)
        succeeded: 0/1 (True/ False)
        return: logic-OR of (this_state state, action) taken ..(this is the reason for action being one-hot)
        """
        return bin(int(start, 2) | int(action[1:], 2)) if succeeded else start  # advance state on success

    @staticmethod
    def get_reward(succeeded: bool) -> float:
        # leaving it here as best_actions_from_each_state utility function for future consumption
        return 1. if succeeded else -1.


class TwoActorPolicy:
    """
    makes a careful selection of two actions: the first one is the 'best' and the second one is the 'second best',
    in the sense that it selects the task with the higher chances of success for the other actor (the first actor is
    currently occupied executing the 'best action')
    """
    def __init__(self):
        pass

    @staticmethod
    def get_action_generator(rewards_table, stage, start_state, horizon):
        return TwoActorPolicy.recursive_get_next_greedy(rewards_table, stage, start_state, horizon)

    @staticmethod
    def get_next_state(start: str, action: str, succeeded: bool) -> str:
        return bin(int(start, 2) | int(action[1:], 2)) if succeeded else start  # advance state on success

    @staticmethod
    def get_two_actor_greedy_action(table: dict, stage: int, state: str, horizon: int):
        """ returns the two best actions for this stage"""
        # print(f' stage, state {stage, state, rewards_table[stage][state]}')
        state_actions = sorted(table[stage][state].items(), key=lambda x: x[1], reverse=True)
        best = state_actions.pop(0)[0]
        if stage == horizon:  # abort with one action at final stage
            return best,
        # exclude from next selection actions from the same actor, or the same action from a different actor
        pick_next_from = (act[0] for act in state_actions if (act[0][0] != best[0] and act[0][1:] != best[1:]))
        sec_best = next(pick_next_from)
        return best, sec_best

    @staticmethod
    def recursive_get_next_greedy(rewards_table: dict, stage: int, this_state: str, horizon: int) -> Optional[tuple]:
        try:  # we are checking for 'end of time-horizon' (no more stages to go through)
            try:  # concurrent assignment to two actors means you need to jump stage to choose for the second actor
                greedy_actions = TwoActorPolicy.get_two_actor_greedy_action(rewards_table, stage, this_state, horizon)
            except KeyError:
                print(f'advancing stage - no such state here')
                stage += 1
                greedy_actions = TwoActorPolicy.get_two_actor_greedy_action(rewards_table, stage, this_state, horizon)

            print(f'greedy for stage: {stage, greedy_actions} from state: {this_state}')
            yield { this_state: (stage, greedy_actions) }
            # yield greedy_actions

            # done with this state-action pair, go for the next state assuming success in both assignments
            next_state = this_state
            for act in greedy_actions:
                next_state = TwoActorPolicy.get_next_state(next_state, act, True)

            yield from TwoActorPolicy.recursive_get_next_greedy(rewards_table, stage + 1, next_state, horizon)
        except KeyError:
            print(f'exit: have run through all stages')
            # yield None


class GreedyPolicy:
    """
    selects only the best next action, regardless of the actor
    """
    def __init__(self):
        pass

    @staticmethod
    def get_action_generator(rewards_table: dict, start: str = None):
        best_action_from_each_state = GreedyPolicy.pick_best_of_stage(rewards_table)
        # print(f'\nonly best actions for each state\n---------------------------')
        # pprint(best_action_from_each_state)

        if start is None:
            [start] = best_action_from_each_state[0].keys()
        else:
            start = start
        return GreedyPolicy.recursive_get_next_greedy(max_table=best_action_from_each_state, this_state=start, stage=0)

    @staticmethod
    def get_next_state(start: str, action: str, succeeded: bool) -> str:
        return bin(int(start, 2) | int(action[1:], 2)) if succeeded else start  # advance state on success

    @staticmethod
    def pick_best_of_stage(rewards_table: dict):
        best_of_stage = {}
        best_of_state = {}
        for stage, stage_table in rewards_table.items():
            for state, state_table in stage_table.items():
                action = max(state_table, key=state_table.get)
                cost = state_table[action]
                # print(f'best action, cost: {stage, state, action, cost}')
                best_of_state[state] = (action, cost)

            best_of_stage[stage] = dict(
                best_of_state)  # make best_actions_from_each_state copy of the dict (mutable is passed by ref)
            best_of_state.clear()
        return best_of_stage

    @staticmethod
    def recursive_get_next_greedy(max_table: dict, stage: int, this_state: str) -> Optional[str]:
        try:
            greedy_action = max_table[stage][this_state][0]
            print(f'greedy for stage: {stage, greedy_action} from state: {this_state}')
            yield greedy_action
            next_state = GreedyPolicy.get_next_state(this_state, greedy_action, True)
            yield from GreedyPolicy.recursive_get_next_greedy(max_table, stage + 1, next_state)
        except KeyError:
            print(f'run through all stages')
            yield None
