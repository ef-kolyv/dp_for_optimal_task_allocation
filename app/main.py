"""
creates REST API to serve the results of a task allocation between a human and a robot, given their probabilities of
carrying out the assigned tasks successfully

the REST call is a POST(): you can change the default values of success probabilities for human and robot by setting
the lists:

- human_success_prob_list: list[float]
- robot_success_prob_list: list[float]


POST Request-body should look like this (orleave empty - these are the default values)
{
    "human_success_prob_list": {
        "transition_probability": [0.9, 0.7, 0.67, 0.63, 0.6]
    },
    "robot_success_prob_list": {
        "transition_probability": [0.7, 0.7, 0.7, 0.7, 0.7]
    }
}
"""

from fastapi import FastAPI
from pydantic import BaseModel
from typing import Union

from assign.two_actor_task_assignment import ProblemSetup, BuildCostMatrix, TwoActorPolicy


app = FastAPI()


class ProbabilityList(BaseModel):
    transition_probability: list[float]


@app.post("/")
async def root(
        human_success_prob_list: Union[ProbabilityList, None] = None,
        robot_success_prob_list: Union[ProbabilityList, None] = None
) -> dict:

    if human_success_prob_list is None:
        human_success_prob_list = [0.9, 0.7, 0.67, .63, .6]
    else:
        # strip the pydantic model
        human_success_prob_list = human_success_prob_list.transition_probability

    if robot_success_prob_list is None:
        robot_success_prob_list = [0.7, 0.7, 0.7, 0.7, 0.7]
    else:
        robot_success_prob_list = robot_success_prob_list.transition_probability

    # basic input testing
    if len(human_success_prob_list) > 5 or len(robot_success_prob_list) > 5:
        return {f'reduce the list len to <= 5 (consider using less states)': list()}

    if len(human_success_prob_list) == 0:
        return {f'got empty list; remove all arguments for default behavior': list()}

    if len(human_success_prob_list) != len(robot_success_prob_list):
        return {f'success probability lists for human and robot must have equal length': list()}

    test_for_probabilities = list(human_success_prob_list + robot_success_prob_list)
    if any([True for x in test_for_probabilities if (x < 0.0 or x > 1.0)]):
        return {f'individual entries are probabilities and should be between 0.0 and 1.0': list()}

    # ---
    # setup problem, construct the cost matrix and return the job assignment
    problem = ProblemSetup(human_success_prob_list, robot_success_prob_list)

    matrix = BuildCostMatrix(problem.state_start, problem.transition_probabilities, problem.actions,
                             problem.time_horizon)
    print(f'edit the pprint in main.py to see the rewards_table with all costs\n--------------------')
    # pprint(matrix.rewards_table)
    policy = TwoActorPolicy()
    action_generator = policy.get_action_generator(matrix.rewards_table, 0, problem.state_start, problem.time_horizon)
    # print(*list(action_generator))

    # parse for beautification
    ass = str()
    for i, phase in enumerate(action_generator):
        assignments = list(phase.values())[0][1]
        print(f'assignments: {assignments}')
        pair = str()
        for assignment in assignments:
            actor = f'actor: {"human" if assignment[0] == "h" else "robot"}\t'
            task = f'task#: {len(assignment[3:])}\n'
            pair += actor + task
        ass += f'Assignment No. {i+1}\n -------------------------\n'
        ass += pair + '\n'

    # print(ass)

    return {f'optimal action sequence from state {int(problem.state_start, 2)}\n\n': ass}
    # return {f'optimal action sequence from state {problem.state_start}': list(action_generator)}


@app.get("/")
async def root():
    return {f'only serving HTTP-POST (to make the sending of transition probability matrices possible)': list()}


# if __name__ == "__main__":

    # human_success_prob_list = [0.9, 0.7, 0.67, .63, .6]
    # robot_success_prob_list = [0.7, 0.7, 0.7, 0.7, 0.7]
    #
    # # ---
    # # setup problem, construct the cost matrix and return the job assignment
    # problem = ProblemSetup(human_success_prob_list, robot_success_prob_list)
    #
    # matrix = BuildCostMatrix(problem.state_start, problem.transition_probabilities, problem.actions,
    #                          problem.time_horizon)
    # print(f'edit the pprint in main.py to see the rewards_table with all costs\n--------------------')
    # policy = TwoActorPolicy()
    # action_generator = policy.get_action_generator(matrix.rewards_table, 0, problem.state_start, problem.time_horizon)
    #
    # # print(f'action-generator: {list(action_generator)}')

    # ass = str()
    # for i, phase in enumerate(action_generator):
    #     assignments = list(phase.values())[0][1]
    #     print(f'assignments: {assignments}')
    #     pair = str()
    #     for assignment in assignments:
    #         actor = f'actor: {"human" if assignment[0] == "h" else "robot"}\t'
    #         task = f'task#: {len(assignment[3:])}\n'
    #         pair += actor + task
    #     ass += f'Assignment No. {i+1}\n -------------------------\n'
    #     ass += pair + '\n'
    #
    # print(ass)





    # from pprint import pprint
    # pprint(matrix.rewards_table)





















