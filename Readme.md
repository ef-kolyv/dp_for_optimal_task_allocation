## Purpose 

Allocate tasks among two actors (for example a human worker and a robot) based on their probability to succeed in 
the execution of each assigned task. Uses Dynamic Programming (DP) to obtain an optimal solution, that is the entire 
sequence of task assignments is optimal with respect to maximizing the probability to succeed in all tasks (a finite 
and small number of tasks is assumed). 

## Modeling and Assumptions 

The tasks can be executed in any order (there are no dependencies between tasks) and can be undertaken by either of 
the two actors - to be executed individually. 
Each of the actors has potentially different probability to succeed in executing each individual task; this 
probability is given as a vector (one probability for each task) and is an input to the app. Constant reward for 
each task is assumed `+1` for success and `-1` for failure. Each action, that is 'assignment of task #1' is 
described by a one-hot vector (1 at the position of the task, when counting from the right) and each state is given 
by the logic-OR of all completed actions.


## Implementation

It is a direct, naive implementation of the DP algorithm, on purpose - used as an exercise recursive solutions. Can 
become slow (with avg. hardware on 2022) for a number of tasks higher than 7-8.   

#to-do: implement with Value Iteration 


## Dependencies 

Uses pure Python (3.9) without any modern features. The only dependencies come from the deployment requirements, 
that is dockerization. To test locally run the uvicorn server and make a `POST-HTTP` on the respective port (`POST` 
because you need to pass the vectors of probability of success for each actor). Check the docstring in `assign/main` 
for en example of the `POST` message. 